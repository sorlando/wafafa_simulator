#ifndef COMMON_H
#define COMMON_H
#include <windows.h>
#include <process.h>
#include <GL\glew.h>
#include <glut.h>
#include <vector>
#include <stdlib.h>
struct color3ub{
unsigned char r;
unsigned char g;
unsigned char b;
};
//static float constants[4];// = {2,25,10,35};
static const color3ub colors[] = {{255, 0, 0}, {0, 255, 0}, {255, 124, 0}, {0, 0, 255},{255, 0, 255},{255, 255, 255},{128, 255, 0},{255, 0, 128},{255, 128, 0},{255, 128, 128}};
#endif