#include "GraphRenderer.h"

std::vector<Graph*> GraphRenderer::graphVect;
HANDLE GraphRenderer::renderMutex;
float * GraphRenderer::constants;
static bool dragged = false;
void GraphRenderer::mouse(int button, int state, int x, int y)
{
	float step = 100.0f / 500.0f;
	if(y > 0 && y <= 125)
	{
		GraphRenderer::constants[0] = step*x;
	}else if(y>125 && y <= 250)
	{
		GraphRenderer::constants[1] = step*x;
	}else if(y > 250 && y <= 375)
	{
		GraphRenderer::constants[2] = step*x;
	}else if(y > 375 && y <= 500)
	{
		GraphRenderer::constants[3] = step*x;
	}
	if(button == GLUT_LEFT_BUTTON && state == GLUT_DOWN)
	{
		dragged = true;
	}
	else
	{
		dragged = false;
	}
	printf("%.2f, %.2f, %.2f, %.2f\n", GraphRenderer::constants[0],GraphRenderer::constants[1],GraphRenderer::constants[2],GraphRenderer::constants[3]);
}

void GraphRenderer::draggedMouse(int x, int y)
{
	float step = 100.0f / 500.0f;
	if(dragged)
	{
		if(y > 0 && y <= 125)
		{
			GraphRenderer::constants[0] = step*x;
		}else if(y>125 && y <= 250)
		{
			GraphRenderer::constants[1] = step*x;
		}else if(y > 250 && y <= 375)
		{
			GraphRenderer::constants[2] = step*x;
		}else if(y > 375 && y <= 500)
		{
			GraphRenderer::constants[3] = step*x;
		}
	}
	printf("%.2f, %.2f, %.2f, %.2f\n", GraphRenderer::constants[0],GraphRenderer::constants[1],GraphRenderer::constants[2],GraphRenderer::constants[3]);
}
void GraphRenderer::display(void) {
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f); // Clear the background of our window to red
	glClear(GL_COLOR_BUFFER_BIT); //Clear the colour buffer (more buffers later on)
	glLoadIdentity(); // Load the Identity Matrix to reset our drawing locations
	gluLookAt(250, 0.0, 450, 250, 0.0, 0.0, 0.0, 1.0, 0.0);
	if(graphVect.size() != 0)
	{
		std::vector<Graph*>::iterator it;
		for(it = graphVect.begin(); it < graphVect.end(); ++it)
		{
			((Graph*)*it)->renderGraph();
		}
	}
	glutSwapBuffers(); // Flush the OpenGL buffers to the window
}
void GraphRenderer::genLists(void)
{
	for(int i = 0; i < 10; ++i)
	{
		vertexLists[i] = glGenLists(1);
		glNewList(vertexLists[i], GL_COMPILE);
		glColor3ub(colors[i].r, colors[i].g, colors[i].b);
		glBegin(GL_POINTS);
		glVertex3f(0.0,0.0,0.0);
		glEnd();
		glEndList();
	}
}
void GraphRenderer::init_graph(void * args, float * c)
{
	GraphRenderer::constants = c;
	ArgsList *argsList = (ArgsList*) args;
	glutInit(&argsList->argc, argsList->argv); // Initialize GLUT
	glutInitDisplayMode (GLUT_DOUBLE); // Set up a basic display buffer (only single buffered for now)
	glutInitWindowSize (width, height); // Set the width and height of the window
	glutInitWindowPosition (100, 100); // Set the position of the window
	glutCreateWindow ("Graph"); // Set the title for the window
	glutIdleFunc (display);
	//glutMouseFunc(mouse);
	//glutMotionFunc(draggedMouse);
	glutDisplayFunc(display); // Tell GLUT to use the method "display" for rendering
	glutReshapeFunc (reshape);
	genLists();
	setup = true;
}
void GraphRenderer::startMain()
{
	glutMainLoop();
}
void GraphRenderer::reshape (int w, int h) {
	glViewport (0, 0, (GLsizei)w, (GLsizei)h);
	glMatrixMode (GL_PROJECTION);
	glLoadIdentity ();
	gluPerspective (60, (GLfloat)w / (GLfloat)h, 0.0, 100000.0);
	glMatrixMode (GL_MODELVIEW);
	//width = w;
	//height = h;
}
bool GraphRenderer::isGraphSetup()
{
	return setup;
}
void GraphRenderer::addGraph(Graph * g)
{
	graphVect.push_back(g);
	g->setVertList(vertexLists[graphVect.size()-1]);
}