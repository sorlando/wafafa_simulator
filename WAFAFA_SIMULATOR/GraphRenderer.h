#ifndef RENDERGRAPH_H
#define RENDERGRAPH_H
#include "common.h"
#include "graph.h"

struct ArgsList{
	int argc;
	char ** argv;
};
class GraphRenderer{
public:
	static const int res = 5000;
	static const int width = 500;
	static const int height = 500;
	GraphRenderer():setup(false){renderMutex = CreateMutex(NULL, FALSE, NULL);};
	~GraphRenderer(){};
	void init_graph(void * args, float * constants);
	void addGraph(Graph * g);
	bool isGraphSetup();
	void startMain();
	static HANDLE renderMutex;
	static float * constants;
private:
	GLuint vertexLists[10];
	static std::vector<Graph*> graphVect;
	void renderDatasets();
	void genLists();
	static void mouse(int button, int state, int x, int y);
	static void draggedMouse(int x, int y);
	bool setup;
	static void display(void);
	static void reshape (int w, int h);
};

#endif