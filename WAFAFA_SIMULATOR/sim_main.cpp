#include <windows.h>
#include <tchar.h>
#include <stdio.h>
#include <strsafe.h>
#include "SimConnect.h"
#include <cmath>
#include <math.h>
#include <conio.h>
#include <process.h>
#include <GL\glew.h>
#include <glut.h>
#include "GraphRenderer.h"
#define WIN32_LEAN_AND_MEAN
#define WIN32_EXTRA_LEAN
static float FREQ = 30;
int     quit = 0;
HANDLE  hSimConnect = NULL;
GraphRenderer rend;
float *constants;
Graph * g;// = new Graph(0,500,-5,5, 0);
Graph * g2;// = new Graph(0, 500, -100, 100, 200);
Graph * g3;// = new Graph(0, 500, -100, 100, -200);
Graph * g4;// = new Graph(0,500,-5,5, 0);
Graph * g5;// = new Graph(0, 500, -100, 100, 200);
Graph * g6;// = new Graph(0, 500, -100, 100, -200);
// Struct to hold vector data
static inline float sign(float x)
{
	return (x<0) ? -1 : (x>0) ? 1 : 0;
}
static float prevAlt = 0;
struct RotVec3f{
	float theta;
	float psi;
	float phi;
};
struct DiffVec3f{
	float q;
	float r;
	float p;
};
struct eulerAngle
{
	float e0;
	float e1;
	float e2;
	float e3;
};

// Struct to hold sensor data.
struct SensorData {
	DiffVec3f acceleration; //ACCELERATION BODY #
	DiffVec3f rotVel; //VELOCITY BODY #
	float alt;//PLANE ALTITUTE
	float lat;//PLANE LATITUDE
	float longitude;//PLANE LONGITUDE
	float pitch;//PLANE PITCH DEGREES
	float bank;//PLANE BANK DEGREES
	float airspeed; //AIRSPEED TRUE
	double elevatorDeflect;
	double rudderDeflect;
	double aileronDeflect;
	double throttlePercent;
};

struct FeedbackData {
	double elevatorDeflect;
	double rudderDeflect;
	double aileronDeflect;
	double throttlePercent;
};

static enum GROUP_ID {
	GROUP0,
};

static enum INPUT_ID {
	INPUT0,
};

static enum EVENT_PDR {
	EVENT_SIM_START,
	EVENT_AUTOPILOT_SWITCH,
};

static enum DATA_DEFINE_ID {
	DATAOUT_DEFINITION,
	DATAIN_DEFINITION,
};

static enum DATA_REQUEST_ID {
	SENSOR_PDR,
	FEEDBACK_PDR,
};

static enum SENSOR_DATA_NAMES {
	DATA_ACCELERATION_X,
	DATA_ACCELERATION_Y,
	DATA_ACCELERATION_Z,
	DATA_ROTVEL_X,
	DATA_ROTVEL_Y,
	DATA_ROTVEL_Z,
	DATA_ALTITUDE,
	DATA_LATITUDE,
	DATA_LONGITUDE,
	DATA_PITCH,
	DATA_BANK,
	DATA_AIRSPEED,
	DATA_ELEVATOR,
	DATA_RUDDER,
	DATA_AILERON,
	DATA_THROTTLE,
};

//static enum DATA_OUTPUTS{
//	DATA_ELEVATOR,
//	DATA_RUDDER,
//	DATA_AILERON,
//	DATA_THROTTLE,
//};
static enum DATA_NAMES {
	DATA_VERTICAL_SPEED,
	DATA_PITOT_HEAT,
};

static RotVec3f pos = {0,0,0}; 
static FeedbackData fb = {0,0,0,0};
static bool autoPilot = false;
static eulerAngle e = {1,0,0,0};
static float goalPitch = 0;
void CALLBACK MyDispatchProcPDR(SIMCONNECT_RECV* pData, DWORD cbData, void *pContext)
{
	HRESULT hr;
	
	switch(pData->dwID)
	{
		case SIMCONNECT_RECV_ID_EVENT:
		{
			SIMCONNECT_RECV_EVENT *evt = (SIMCONNECT_RECV_EVENT*)pData;
			switch(evt->uEventID)
			{
				case EVENT_SIM_START:
					hr = SimConnect_RequestDataOnSimObject(hSimConnect, SENSOR_PDR, DATAOUT_DEFINITION, SIMCONNECT_OBJECT_ID_USER, SIMCONNECT_PERIOD_VISUAL_FRAME);
					hr = SimConnect_RequestDataOnSimObject(hSimConnect, FEEDBACK_PDR, DATAIN_DEFINITION, SIMCONNECT_OBJECT_ID_USER, SIMCONNECT_PERIOD_VISUAL_FRAME);
					fb.aileronDeflect = 0.0f;
					fb.elevatorDeflect = 0.0f;
					fb.rudderDeflect = 0.0f;
					fb.throttlePercent = 100;
					pos.phi = pos.psi = pos.theta = 0;
					e.e0 = 1;
					e.e1 = 0;
					e.e2 = 0;
					e.e3 = 0;
					hr = SimConnect_SetDataOnSimObject(hSimConnect, DATAIN_DEFINITION, SIMCONNECT_OBJECT_ID_USER, 0,0,sizeof(FeedbackData), &fb);
					break;
				case EVENT_AUTOPILOT_SWITCH:
					autoPilot = !autoPilot;
					printf("AutoPilot %d\n",autoPilot);
				default:
				   break;
			}
			break;
		}

		case SIMCONNECT_RECV_ID_SIMOBJECT_DATA:
		{
			SIMCONNECT_RECV_SIMOBJECT_DATA *pObjData = (SIMCONNECT_RECV_SIMOBJECT_DATA*)pData;
			
			switch(pObjData->dwRequestID)
			{
				case SENSOR_PDR:
				{
					SensorData *d = (SensorData*)&pObjData->dwData;
					//printf("Acceleration %f, %f, %f\n RotVel %f, %f, %f\n Alt %f\n Long %f\n Lat %f\n Pitch %f\n Bank %f\n Airspeed %f\n", d->acceleration.x, d->acceleration.y, d->acceleration.z, d->rotVel.x,
					//	d->rotVel.y, d->rotVel.z, d->alt, d->longitude, d->lat, d->pitch, d->bank, d->airspeed);+
					/*d->rotVel.p/= FREQ;
					d->rotVel.q/= FREQ;
					d->rotVel.r/= FREQ;*/
					g->updateGraph(d->airspeed);
					g2->updateGraph(d->alt);
					
					e.e0 -= (1.0f/(2.0f*FREQ))*(e.e1*d->rotVel.p + e.e2 * d->rotVel.q + e.e3*d->rotVel.r);
					e.e1 += (1.0f/(2.0f*FREQ))*(e.e0*d->rotVel.p + e.e2 * d->rotVel.r - e.e3*d->rotVel.q);
					e.e2 += (1.0f/(2.0f*FREQ))*(e.e0*d->rotVel.q + e.e3 * d->rotVel.p - e.e1*d->rotVel.r);
					e.e3 += (1.0f/(2.0f*FREQ))*(e.e0*d->rotVel.r + e.e1 * d->rotVel.q - e.e2*d->rotVel.p);
					pos.theta = asin(-2.0f*(e.e1*e.e3 - e.e0*e.e2));
					pos.phi = atan2(2.0f*(e.e0*e.e1 + e.e2*e.e3), 1.0f - 2.0f*(e.e1*e.e1 + e.e2*e.e2));
					pos.psi = atan2(2.0f*(e.e0*e.e3 + e.e1*e.e2), 1.0f - 2.0f*(e.e2*e.e2 + e.e3*e.e3));
					//pos.phi += d->rotVel.p + sin(pos.phi/FREQ)*tan(pos.theta/FREQ)*d->rotVel.q + cos(pos.phi/FREQ)*tan(pos.theta/FREQ)*d->rotVel.r;
					//pos.theta += d->rotVel.q*cos(pos.phi/FREQ) - d->rotVel.r*sin(pos.phi/FREQ);
					//pos.psi += d->rotVel.q*sin(pos.phi/FREQ)/cos(pos.theta/FREQ) + d->rotVel.r*cos(pos.phi/FREQ)/cos(pos.theta/FREQ);

					//pos.phi = fmod(pos.phi + 3.1415926535f/2, 2*3.1415926535f) - 3.1415926535f/2;
					//printf("%f\n",d->rotVel.x*cos(-pos.z*2*3.1415926535*1/400));
					//printf("%f, %f, %f: %f %f\n", pos.phi, pos.theta, pos.psi, abs(d->pitch-pos.theta), abs(d->bank - pos.phi));
					if(autoPilot)
					{
						float alt = prevAlt - d->alt;
						prevAlt = d->alt;
						fb.aileronDeflect = d->aileronDeflect + d->rotVel.p/constants[0] + pos.phi/constants[1];
						fb.rudderDeflect = d->rudderDeflect;// - d->rotVel.z/20;
						//fb.elevatorDeflect = d->rotVel.q/constants[2] + (pos.theta - min((constants[4] - d->airspeed),0)/6.0f)/constants[3] ;
						fb.elevatorDeflect = max(-(constants[4] - d->airspeed)/5.0f + d->rotVel.q/6.0f + alt, -.13);
						fb.throttlePercent = max(0,min(80 + (constants[5] - d->alt)/10.0f - alt/6.0f,100.0f));
						printf("Airspeed: %f, Altitude: %f, Throttle: %f, Elevator: %f,%f\n", d->airspeed, d->alt, fb.throttlePercent, fb.elevatorDeflect,d->elevatorDeflect);
						hr = SimConnect_SetDataOnSimObject(hSimConnect, DATAIN_DEFINITION, SIMCONNECT_OBJECT_ID_USER, 0,0,sizeof(FeedbackData), &fb);
					}
					//g->updateGraph(pos.phi);
					//g2->updateGraph(pos.psi);
					g3->updateGraph(pos.theta);
					//g->updateGraph(d->rotVel.p);
					//g2->updateGraph(d->rotVel.q);
					//g3->updateGraph(d->rotVel.r);
					
					break; 
				}
				case FEEDBACK_PDR:
				{
					FeedbackData *d = (FeedbackData*)&pObjData->dwData;
					//printf("Ele: %f, Ail: %f, Rud: %f, Thr: %2.1f \n", d->elevatorDeflect, d->aileronDeflect, d->rudderDeflect, d->throttlePercent);
				}
				default:
				   break;
			}
			break;
		}


		case SIMCONNECT_RECV_ID_QUIT:
		{
			quit = 1;
			break;
		}

		default:
			printf("\nUnknown dwID: %d",pData->dwID);
			break;
	}
}

void __cdecl testTaggedDataRequest(void * data)
{
	HRESULT hr;
	g = new Graph(0,500, 0, 500, 0, colors[0]);
	g2 = new Graph(0, 500, 0, 10000, 150, colors[1]);
	g3 = new Graph(0, 500, -6, 6, -150, colors[2]);
	rend.addGraph(g);
	rend.addGraph(g2);
	rend.addGraph(g3);
	if (SUCCEEDED(SimConnect_Open(&hSimConnect, "Tagged Data", NULL, 0, 0, 0)))
	{
		printf("\nConnected to Flight Simulator!");   
		hr = SimConnect_MapClientEventToSimEvent(hSimConnect, EVENT_AUTOPILOT_SWITCH);
		hr = SimConnect_AddClientEventToNotificationGroup(hSimConnect, GROUP0, EVENT_AUTOPILOT_SWITCH);
		hr = SimConnect_SetNotificationGroupPriority(hSimConnect, GROUP0, SIMCONNECT_GROUP_PRIORITY_HIGHEST);
		hr = SimConnect_MapInputEventToClientEvent(hSimConnect, INPUT0, "space", EVENT_AUTOPILOT_SWITCH);
		hr = SimConnect_SetInputGroupState(hSimConnect, GROUP0, SIMCONNECT_STATE_ON);
		// Set up the data definition, ensuring that all the elements are in Float32 units, to
		// match the StructDatum structure
		hr = SimConnect_AddToDataDefinition(hSimConnect, DATAOUT_DEFINITION, "ACCELERATION BODY X", "Feet per second squared", SIMCONNECT_DATATYPE_FLOAT32, 0, DATA_ACCELERATION_X);
		hr = SimConnect_AddToDataDefinition(hSimConnect, DATAOUT_DEFINITION, "ACCELERATION BODY Y", "Feet per second squared", SIMCONNECT_DATATYPE_FLOAT32, 0, DATA_ACCELERATION_Y);
		hr = SimConnect_AddToDataDefinition(hSimConnect, DATAOUT_DEFINITION, "ACCELERATION BODY Z", "Feet per second squared", SIMCONNECT_DATATYPE_FLOAT32, 0, DATA_ACCELERATION_Z);
		hr = SimConnect_AddToDataDefinition(hSimConnect, DATAOUT_DEFINITION, "ROTATION VELOCITY BODY X", "radians per second", SIMCONNECT_DATATYPE_FLOAT32, 0, DATA_ROTVEL_X);
		hr = SimConnect_AddToDataDefinition(hSimConnect, DATAOUT_DEFINITION, "ROTATION VELOCITY BODY Y", "radians per second", SIMCONNECT_DATATYPE_FLOAT32, 0, DATA_ROTVEL_Y);
		hr = SimConnect_AddToDataDefinition(hSimConnect, DATAOUT_DEFINITION, "ROTATION VELOCITY BODY Z", "radians per second", SIMCONNECT_DATATYPE_FLOAT32, 0, DATA_ROTVEL_Z);
		hr = SimConnect_AddToDataDefinition(hSimConnect, DATAOUT_DEFINITION, "PLANE ALTITUDE", "Feet", SIMCONNECT_DATATYPE_FLOAT32, 0, DATA_ALTITUDE);
		hr = SimConnect_AddToDataDefinition(hSimConnect, DATAOUT_DEFINITION, "PLANE LATITUDE", "Radians", SIMCONNECT_DATATYPE_FLOAT32, 0, DATA_LATITUDE);
		hr = SimConnect_AddToDataDefinition(hSimConnect, DATAOUT_DEFINITION, "PLANE LONGITUDE", "Radians", SIMCONNECT_DATATYPE_FLOAT32, 0, DATA_LONGITUDE);
		hr = SimConnect_AddToDataDefinition(hSimConnect, DATAOUT_DEFINITION, "PLANE PITCH DEGREES", "Radians", SIMCONNECT_DATATYPE_FLOAT32, 0, DATA_PITCH);
		hr = SimConnect_AddToDataDefinition(hSimConnect, DATAOUT_DEFINITION, "PLANE BANK DEGREES", "Radians", SIMCONNECT_DATATYPE_FLOAT32, 0, DATA_BANK);
		hr = SimConnect_AddToDataDefinition(hSimConnect, DATAOUT_DEFINITION, "AIRSPEED TRUE", "knots", SIMCONNECT_DATATYPE_FLOAT32, 0, DATA_AIRSPEED);
		hr = SimConnect_AddToDataDefinition(hSimConnect, DATAOUT_DEFINITION, "ELEVATOR POSITION", "Position", SIMCONNECT_DATATYPE_FLOAT64, 0, DATA_ELEVATOR);
		hr = SimConnect_AddToDataDefinition(hSimConnect, DATAOUT_DEFINITION, "RUDDER POSITION", "Position", SIMCONNECT_DATATYPE_FLOAT64, 0, DATA_RUDDER);
		hr = SimConnect_AddToDataDefinition(hSimConnect, DATAOUT_DEFINITION, "AILERON POSITION", "Position", SIMCONNECT_DATATYPE_FLOAT64, 0, DATA_AILERON);
		hr = SimConnect_AddToDataDefinition(hSimConnect, DATAOUT_DEFINITION, "GENERAL ENG THROTTLE LEVER POSITION:1", "percent",SIMCONNECT_DATATYPE_FLOAT64,0, DATA_THROTTLE);

		//Set up data definition to set elevator, rudder, aleron, throttle
		hr = SimConnect_AddToDataDefinition(hSimConnect, DATAIN_DEFINITION, "ELEVATOR POSITION", "Position", SIMCONNECT_DATATYPE_FLOAT64, 0, DATA_ELEVATOR);
		hr = SimConnect_AddToDataDefinition(hSimConnect, DATAIN_DEFINITION, "RUDDER POSITION", "Position", SIMCONNECT_DATATYPE_FLOAT64, 0, DATA_RUDDER);
		hr = SimConnect_AddToDataDefinition(hSimConnect, DATAIN_DEFINITION, "AILERON POSITION", "Position", SIMCONNECT_DATATYPE_FLOAT64, 0, DATA_AILERON);
		hr = SimConnect_AddToDataDefinition(hSimConnect, DATAIN_DEFINITION, "GENERAL ENG THROTTLE LEVER POSITION:1", "percent",SIMCONNECT_DATATYPE_FLOAT64,0, DATA_THROTTLE);


		// Request a simulation start event
		hr = SimConnect_SubscribeToSystemEvent(hSimConnect, EVENT_SIM_START, "SimStart");

		while( 0 == quit )
		{
			SimConnect_CallDispatch(hSimConnect, MyDispatchProcPDR, NULL);
			Sleep(1);
		} 

		hr = SimConnect_Close(hSimConnect);
	}
}
void setFreq(void * data)
{
	while(1)
	{
		scanf_s("%f %f", &constants[4], &constants[5]);
		//printf("%.2f %.2f %.2f %.2f\n", constants[0], constants[1], constants[2], constants[3]);
		Sleep(30);
	}
}

int __cdecl main(int argc, char* argv[])
{
	ArgsList args;

	constants = (float*)malloc(sizeof(float) * 6);
	constants[0] = 8.4f;
	constants[1] = 8.8f;
	constants[2] = 8.6f;
	constants[3] = 5.0f;
	constants[4] = 134.0f;
	constants[5] = 1600.0f;

	args.argc = argc;
	args.argv = argv;
	rend.init_graph(&args, constants);
	_beginthread(setFreq,NULL,NULL);
	_beginthread(testTaggedDataRequest, NULL, NULL);
	rend.startMain();
	free(constants);
	return 0;
}