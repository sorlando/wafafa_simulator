#include "graph.h"
#include "GraphRenderer.h"
Graph::Graph(float domainMin, float domainMax, float rangeMin, float rangeMax, float scale, struct color3ub color)
{
	points = new DataPoint[GraphRenderer::width];
	xPoints = new float[GraphRenderer::width];
	yPoints = new float[GraphRenderer::width];
	this->color = color;
	//xPoints = (float*) _aligned_malloc(sizeof(float)*GraphRenderer::width,7);
	//yPoints = (float*) _aligned_malloc(sizeof(float)*GraphRenderer::width,7);
	domain = new Bounds(domainMin,domainMax);
	range = new Bounds(rangeMin, rangeMax);
	this->scale = scale;
	//float xstep = abs((domain->upperBound - domain->lowerBound))/GraphRenderer::width;
	float xstep = 1;
	for(int i = 0; i < GraphRenderer::width; ++i)
	{
		xPoints[i] = xstep * i;
		yPoints[i] = scale;
		points[i].x = &xPoints[i];
		points[i].y = &yPoints[i];

		//points[i].x = rand()/(float(RAND_MAX))*1000-500;
		//points[i].y = rand()/(float(RAND_MAX))*1000-500;
	}

}
void Graph::setVertList(GLuint x)
{
	this->vertexList = x;
}
Graph::~Graph()
{
	delete domain;
	delete range;
	delete points;
	domain = range = NULL;
	points = NULL;
}

void Graph::updateGraph(float val)
{
	float yScale = ((float)GraphRenderer::width)/abs(range->upperBound - range->lowerBound);
	for(int i = 0; i < GraphRenderer::width; ++i)
	{
		xPoints[i] -= 1;
	}
	for(int i = 0; i < GraphRenderer::width; ++i)
	{
		if(xPoints[i] < 0)
		{
			xPoints[i] = GraphRenderer::width;
			yPoints[i] = val*yScale + scale;
			break;
		}
	}
}
void Graph::renderGraph()
{
	//find start of list
	unsigned int beginIndex = 0;
	for(int i = 0; i < GraphRenderer::width; ++i)
	{
		if(*this->points[i].x <= 0)
		{
			beginIndex = i;
			break;
		}
	}
	//glColor3ub(colors[i].r, colors[i].g, colors[i].b);
	glBegin(GL_LINE_STRIP);
	glColor3ub(color.r, color.g, color.b);
	//glVertex3f(0.0,0.0,0.0);
	for(unsigned int i = beginIndex; i < GraphRenderer::width; ++i)
	{
		//this->points[i].renderPoint(&vertexList);
		//printf("%f", points[i].x);
		glVertex3f(*this->points[i].x, *this->points[i].y,0);
	}
	for(unsigned int i = 0; i < beginIndex; ++i)
	{
		//this->points[i].renderPoint(&vertexList);
		//printf("%f", points[i].x);
		glVertex3f(*this->points[i].x, *this->points[i].y,0);
	}
	glEnd();
}
void DataPoint::renderPoint(GLuint * vertList)
{
	glPushMatrix();
	glTranslatef(*x,*y,0);
	glCallList(*vertList);
	//glScalef(.25,.25,.25);
	glPopMatrix();
}