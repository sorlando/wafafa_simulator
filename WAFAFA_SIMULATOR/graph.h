#ifndef GRAPH_H
#define GRAPH_H
#include "common.h"


struct Bounds{
	float lowerBound;
	float upperBound;
	Bounds(float min, float max):lowerBound(min),upperBound(max){}
	Bounds():lowerBound(0), upperBound(0){}
};

struct DataPoint{
	float *x;
	float *y;
	DataPoint(float *x, float *y): x(x), y(y){}
	DataPoint():x(0), y(0){}
	void renderPoint(GLuint * vertList);
};

class Graph{
public:
	Graph(float domainMin, float domainMax, float rangeMin, float rangeMax, float scale, struct color3ub color);
	~Graph();
	void updateGraph(float val);
	void renderGraph(void);
	void setVertList(GLuint);
private:
	Bounds * domain;
	Bounds * range;	
	GLuint vertexList;
	float * xPoints;
	float * yPoints;
	DataPoint * points;
	struct color3ub color;
	float scale;

};
#endif